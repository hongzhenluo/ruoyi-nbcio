package com.ruoyi.workflow.mapper;

import com.ruoyi.workflow.domain.WfDdFlow;
import com.ruoyi.workflow.domain.vo.WfDdFlowVo;
import com.ruoyi.common.core.mapper.BaseMapperPlus;

/**
 * 钉钉流程Mapper接口
 *
 * @author nbacheng
 * @date 2023-11-29
 */
public interface WfDdFlowMapper extends BaseMapperPlus<WfDdFlowMapper, WfDdFlow, WfDdFlowVo> {

}
